
     _____     _____   _____   _   ______                  
    /  _  \   | ____| |  _  \ | | /  ___/                        
    | | | |   | |__   | | | | | | | |____
    | | | |   |  __|  | | | | | |  \__   \
    | |_| |_  | |___  | |_| | | |  ___|  |
    \_______| |_____| |_____/ |_| /_____/


[![Build Status](https://travis-ci.org/loveyacper/Qedis.svg?branch=master)](https://travis-ci.org/loveyacper/Qedis)

[看中文说明请点我](README.zh.md)

A C++11 implementation of distributed redis server, use Leveldb for persist storage.(including cluster)

## Requirements
* C++11 & CMake
* Linux or OS X

## Cluster Features
 Use zookeeper for leader election, to reach high availability.

 Of course you can also use redis-sentinel.

 See details in [cluster Readme](QCluster/README.md), still in development.

## Fully compatible with redis
 You can test Qedis with redis-cli, redis-benchmark, or use redis as master with Qedis as slave or conversely, it also can work with redis sentinel.
 In a word, Qedis is full compatible with Redis.

## Support module for write your own extensions
 Qedis supports module now, still in progress, much work to do.
 I added three commands(ldel, skeys, hgets) for demonstration.

## Persistence: Not limited to memory
 Leveldb can be configured as backend for Qedis.

## Support LRU cache
 When memory is low, you can make Qedis to free memory by evict some key according to LRU.

## Master-slave Replication, transaction, RDB/AOF, slow log, publish-subscribe
 Qedis supports them all :-)
 
## Command List
#### show all supported commands list, about 140 commands
- cmdlist

## TODO
* Support lua
* Golang Cluster client

